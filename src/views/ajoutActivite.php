<?php include("templates/header.php") ?>
<main class="container">
    <!-- Fil d'ariane -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-transparent">
            <li class="breadcrumb-item"><a href="#">Journal</a>
            <li class="breadcrumb-item"><a href="#">Cours 1</a></li>
            <li class="breadcrumb-item"><a href="semaine.php">Semaine 1</a></li>
            <li class="breadcrumb-item active" aria-current="page">Ajouter une activité</li>
        </ol>
    </nav>
    <!-- Titre de la page -->
    <h1 class="mt-4 mb-5 h3">Ajouter une activité à mon journal</h1>
    <!-- Formulaire d'ajout d'activité -->
    <!-- @TODO Compléter le formulaire pour envoyer les données -->
    <form action="confirmation.php" method="post">
        <!-- L1 : Lieu, date -->
        <div class="row">
            <div class="col-md-8 mb-3">
                <label for="lieu">Lieu</label>
                <input type="text" class="form-control" id="lieu" name="activite[lieu]" value="Centre Sportif" required>
            </div>
            <div class="col-md-4 mb-3">
                <label for="date">Date</label>
                <!-- VL Ne pas oublier la date comme elle est utilisée plus tard ! -->
                <input type="date" class="form-control" id="date" name="activite[date]"required>
            </div>
        </div>
        <!-- L2 : Avec qui -->
        <div class="form-group">
            <label for="partenaires">Avec qui</label>
            <input type="text" class="form-control" id="partenaires" name="activite[partenaires]" value="Nathalie" required>
        </div>
        <!-- L3 : Activité -->
        <datalist id="activites">
            <option value="Natation">
            <option value="Vélo stationnaire">
            <option value="Ski">
        </datalist>
        <div class="form-group">
            <label for="activite">Activité</label>
            <input type="text" class="form-control" id="activite" name="activite[activite]" value="Natation" list="activites" required>
        </div>
        <!-- L4 : Déterminants de la condition physique -->
        <datalist id="determinants">
            <option value="cardio">
            <option value="endurance musculaire">
        </datalist>
        <div class="form-group">
            <label for="determinants">Déterminant(s) de la condition physique</label>
            <input type="text" class="form-control" id="determinants" name="activite[determinants]" value="cardio" list="determinants" required>
        </div>
        <!-- L5 : Intensité, durée -->
        <div class="row">
            <div class="col-md-8 mb-3">
                <label for="intensite">Intensité de l'effort</label>
                <select class="form-control" id="intensite" name="activite[intensite]" required>
                    <option value="0">0 Aucun effort</option>
                    <option value="1">1 Très faible</option>
                    <option value="2">2 Faible</option>
                    <option value="3">3 Modéré</option>
                    <option value="4">4 Un peu difficile</option>
                    <option value="5">5 Difficile</option>
                    <option value="6">6 Plus difficile</option>
                    <option value="7">7 Très difficile</option>
                    <option value="8">8 Très, très difficile</option>
                    <option value="9">9 Extrêmement difficile</option>
                    <option value="10">10 Maximal</option>
                </select>
            </div>
            <div class="col-md-4 mb-3">
                <label for="duree">Durée (min)</label>
                <input type="number" class="form-control" id="duree" name="activite[duree]" value="30" min="0" max="500" step="5" required>
            </div>
        </div>
        <!-- L6 : Effet(s) ressenti(s) -->
        <div class="form-group">
            <label for="effets">Effet(s) ressenti(s)</label>
            <div id="effets" class="form-group ml-2">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="activite[effet[douleur]]" value="douleur"
                           id="effetDouleur">
                    <label class="form-check-label" for="effetDouleur">
                        Douleur
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="activite[effet[concentration]]" value="concentration"
                           id="effetConcentration">
                    <label class="form-check-label" for="effetConcentration">
                        Augmentation de la concentration
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="activite[effet[stress]]" value="stress"
                           id="effetStress">
                    <label class="form-check-label" for="effetStress">
                        Diminution du stress
                    </label>
                </div>
            </div>
        </div>
        <!-- L7 : Facteur(s) de motivation -->
        <div class="form-group">
            <label for="motivation">Facteur(s) de motivation</label>
            <input type="text" class="form-control" id="motivation" name="activite[motivation]" value="Plaisir" list="facteursMotiv" required>
        </div>
        <!-- L8 : Plaisir -->
        <div class="form-group">
            <label for="plaisir">Plaisir</label>
            <input type="number" class="form-control" id="plaisir" name="activite[plaisir]" value="3" min="0" max="5" step="0.5" required>
        </div>
        <!-- L9 : Boutons -->
        <div class="form-group mt-5">
            <button class="btn btn-primary mr-2" type="submit">Ajouter</button>
            <button class="btn btn-secondary" type="reset">Annuler</button>
        </div>
    </form>
</main>
<?php include("templates/footer.php") ?>
