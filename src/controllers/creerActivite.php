<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
require_once __DIR__ . "/../models/Activite.php";

/**
 * Class creerActivite
 */
class creerActivite
{
    /**
     * Retourne la liste des activites qui est dans $_SESSION
     *
     * @return array
     */
    private static function getActivites(): array
    {
        // Créer une nouveau tableau qui contiendra les activités s'il n'existe pas
        if (!isset($_SESSION["activites"])) {
            $_SESSION["activites"] = [];
        }
        return $_SESSION["activites"];
    }

    /*TODO Compléter la méthode de validation
    Critères de validation :
    - Accepter lettres, ",", ";" "-" "'" pour les champs texte */
    /**
     * vérifi si la chaine reçu en parametre est valide selon des critères choisis
     *
     * @param string $chaine
     * @return bool
     */
    public static function estValide(string $chaine): bool
    {
        return preg_match("/^[A-z ,.\'\-\;]+$/", $chaine); //Excellent :)
    }


    /**
     * Ajoute une nouvelle activitée
     *
     * @param array $details
     * @return bool
     */
    public static function ajoutNouvelleActivite(array $details): bool
    {
        $creation = true;
        //@TODO Ajouter les instructions pour valider les données de l'activité ($details) à l'aide de la méthode estValide
        if(!self::estValide($details["lieu"])){
            $creation = false;
        }
        if(!self::estValide($details["partenaires"])){
            $creation = false;
        }
        if(!self::estValide($details["activite"])){
            $creation = false;
        }
        if(!self::estValide($details["determinants"])){
            $creation = false;
        }
        if(!self::estValide($details["motivation"])){
            $creation = false;
        }

        //Si valide, créer l'activité
        if($creation){
            //Revoir le constructeur ou modifier cet appel
            $activite = new Activite($details);
        }


        //@TODO Si valide, l'ajouter au tableau des activités
        if($creation){
            array_push($_SESSION["activites"], $activite);
        }

        //Retourner vrai si l'activité a été ajoutée, faux sinon
        return $creation;
    }
}