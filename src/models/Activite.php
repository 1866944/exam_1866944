<?php


/**
 *
 * Crée une activitée avec toutes ses propriétés
 *
 * Class Activite
 */
class Activite
{
    private string $lieu;
    private string $partenaires;
    private string $activite;
    private string $determinants;
    private string $intensite;
    private int $duree;
    private string $effet;
    private string $motivation;
    private float $plaisir;

    /**
     * Activite constructor.
     * @param string $lieu
     * @param string $partenaires
     * @param string $activite
     * @param string $determinants
     * @param string $intensite
     * @param int $duree
     * @param string $effet
     * @param string $motivation
     * @param float $plaisir
     */
    //VL Revoir le constructeur ou modifier l'appel
    public function __construct(string $lieu, string $partenaires, string $activite, string $determinants, string $intensite, int $duree, string $effet, string $motivation, float $plaisir)
    {
        $this->lieu = $lieu;
        $this->partenaires = $partenaires;
        $this->activite = $activite;
        $this->determinants = $determinants;
        $this->intensite = $intensite;
        $this->duree = $duree;
        $this->effet = $effet;
        $this->motivation = $motivation;
        $this->plaisir = $plaisir;
    }

    /**
     * @return string
     */
    public function getLieu(): string
    {
        return $this->lieu;
    }

    /**
     * @param string $lieu
     */
    public function setLieu(string $lieu): void
    {
        $this->lieu = $lieu;
    }

    /**
     * @return string
     */
    public function getPartenaires(): string
    {
        return $this->partenaires;
    }

    /**
     * @param string $partenaires
     */
    public function setPartenaires(string $partenaires): void
    {
        $this->partenaires = $partenaires;
    }

    /**
     * @return string
     */
    public function getActivite(): string
    {
        return $this->activite;
    }

    /**
     * @param string $activite
     */
    public function setActivite(string $activite): void
    {
        $this->activite = $activite;
    }

    /**
     * @return string
     */
    public function getDeterminants(): string
    {
        return $this->determinants;
    }

    /**
     * @param string $determinants
     */
    public function setDeterminants(string $determinants): void
    {
        $this->determinants = $determinants;
    }

    /**
     * @return string
     */
    public function getIntensite(): string
    {
        return $this->intensite;
    }

    /**
     * @param string $intensite
     */
    public function setIntensite(string $intensite): void
    {
        $this->intensite = $intensite;
    }

    /**
     * @return int
     */
    public function getDuree(): int
    {
        return $this->duree;
    }

    /**
     * @param int $duree
     */
    public function setDuree(int $duree): void
    {
        $this->duree = $duree;
    }

    /**
     * @return string
     */
    public function getEffet(): string
    {
        return $this->effet;
    }

    /**
     * @param string $effet
     */
    public function setEffet(string $effet): void
    {
        $this->effet = $effet;
    }

    /**
     * @return string
     */
    public function getMotivation(): string
    {
        return $this->motivation;
    }

    /**
     * @param string $motivation
     */
    public function setMotivation(string $motivation): void
    {
        $this->motivation = $motivation;
    }

    /**
     * @return int
     */
    public function getPlaisir(): int
    {
        return $this->plaisir;
    }

    /**
     * @param int $plaisir
     */
    public function setPlaisir(int $plaisir): void
    {
        $this->plaisir = $plaisir;
    }

}