<?php
require_once __DIR__ . "/../../src/controllers/creerActivite.php";

use PHPUnit\Framework\TestCase;

class creerActiviteTest extends TestCase
{
    static function setUpBeforeClass(): void
    {
        $_SESSION = array();
    }

    private const RETURN_TRUE = "Nathalie, Jean-Marc et 'tite laine";
    private const RETURN_FALSE = "33 Cité-des-Jeunes ";

    //@TODO implémenter ce test
    public function testEstValideChaineValide(): void
    {
        $this->assertEquals(true, creerActivite::estValide(self::RETURN_TRUE));
    }

    //@TODO implémenter ce test
    public function testEstValideChaineInValide(): void
    {
        $this->assertEquals(false, creerActivite::estValide(self::RETURN_FALSE));
    }

    public function tearDown(): void
    {
        $_SESSION = array();
    }
}
